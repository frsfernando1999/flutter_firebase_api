import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/pages/product_edit_page/product_edit_page.dart';
import 'package:shop/utils/custom_routes.dart';

import './models/models.dart';
import './utils/app_routes.dart';
import './pages/pages.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => FirebaseUser()),
        ChangeNotifierProxyProvider<FirebaseUser, ProductList>(
          create: (_) => ProductList(),
          update:
              (BuildContext context, FirebaseUser auth, ProductList? previous) {
            return ProductList(token: auth.tokenId);
          },
        ),
        ChangeNotifierProxyProvider<FirebaseUser, OrderList>(
          create: (_) => OrderList(),
          update:
              (BuildContext context, FirebaseUser auth, OrderList? previous) {
            return OrderList(token: auth.tokenId, userId: auth.userId);
          },
        ),
        ChangeNotifierProvider(create: (_) => Cart()),
      ],
      child:
       MaterialApp(
        title: 'Shop',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          pageTransitionsTheme: PageTransitionsTheme(
            builders: {
              TargetPlatform.android : CustomPageTransitionsBuilder(),
              TargetPlatform.iOS :  CustomPageTransitionsBuilder(),
            }
          ),
          textTheme: const TextTheme(
            subtitle2: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w400,
            ),
          ),
          primarySwatch: Colors.purple,
        ),

        routes: {
          AppRoutes.splashPage: (context) => const SplashPage(),
          AppRoutes.loginPage: (context) => const LoginPage(),
          AppRoutes.signUpPage: (context) => const SignUpPage(),
          AppRoutes.homePage: (context) => const HomePage(),
          AppRoutes.productDetails: (context) => const ProductDetailPage(),
          AppRoutes.purchasePage: (context) => const PurchasePage(),
          AppRoutes.ordersPage: (context) => const OrdersPage(),
          AppRoutes.productManagementPage: (
              context) => const ProductManagementPage(),
          AppRoutes.productEditPage: (context) => const ProductEditPage(),
          AppRoutes.confirmEmail: (context) => const EmailConfirmationPage(),
        },
      ),
    );
  }
}
