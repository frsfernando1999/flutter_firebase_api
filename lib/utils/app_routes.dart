class AppRoutes{
  static const splashPage = '/';
  static const loginPage ='/login-page';
  static const signUpPage = '/sign-up-page';
  static const homePage = '/homePage';
  static const productEditPage = '/product-edit-page';
  static const productDetails = '/product-details';
  static const purchasePage = '/purchase-page';
  static const ordersPage = '/orders-page';
  static const productManagementPage = '/product-management-page';
  static const confirmEmail = 'confirm-email-page';

}