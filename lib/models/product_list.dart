import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './models.dart';

class ProductList with ChangeNotifier {
  List<Product> items = [];
  List<Product> firebaseItems = [];
  bool loading = false;
  String? token;

  ProductList({this.token}){
    if(token != null) {
      loadProducts(token);
    }
  }


  Future<void> loadProducts(token) async {
    loading = true;
    notifyListeners();
    try{
    http.Response list = await http.get(Uri.parse(
        'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com/products.json?auth=$token'))
      ..body;
    var testList = (jsonDecode(list.body)) as Map;
    for (var entry in testList.entries) {
      firebaseItems.add(Product(
          id: entry.key,
          title: entry.value['title'],
          description: entry.value['description'],
          price: entry.value['price'],
          imageUrl: entry.value['imageUrl'],
          isFavorite: entry.value['isFavorite']));
    }
    items = firebaseItems;
    }
    catch(e){
      throw('Erro 1');
    }finally{
    loading = false;
    notifyListeners();
    }
  }

  void toggleFavorites(int selectedValue) {
    if (selectedValue == 1) {
      items = firebaseItems;
    } else {
      items =
          firebaseItems.where((product) => product.isFavorite == true).toList();
    }
    notifyListeners();
  }

  void addProduct(Product product) {
    items.add(product);
    notifyListeners();
  }

  void removeProduct(String productId) async {
    loading = true;
    notifyListeners();
    try{
      await http.delete(Uri.parse(
          'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com/products/$productId.json'));
      items.removeWhere((element) => element.id == productId);
    notifyListeners();
    }catch (e){
      // TODO
      print('Erro 5');
    }finally{
      loading = false;
      notifyListeners();
    }
  }
}
