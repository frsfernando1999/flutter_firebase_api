import 'dart:math';

import 'package:flutter/material.dart';

import './models.dart';

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemsCount {
    return items.length;
  }

  int get cartTotalItems {
    int totalItems = 0;
    for (var entries in items.entries) {
      totalItems += entries.value.quantity;
    }
    return totalItems;
  }

  double get totalAmount {
    double total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void addItem(Product product) {
    if (_items.containsKey(product.id)) {
      _items.update(
          product.id,
          (item) => CartItem(
                id: item.id,
                productId: item.productId,
                title: item.title,
                quantity: item.quantity + 1,
                price: item.price,
              ));
    } else {
      _items.putIfAbsent(
          product.id,
          () => CartItem(
                id: Random().nextInt(100000).toString(),
                productId: product.id,
                title: product.title,
                quantity: 1,
                price: product.price,
              ));
    }
    notifyListeners();
  }

  void removeSingleItem(String productId) {
    if (_items[productId]?.quantity == 1) {
      _items.remove(productId);
    } else {
      _items.update(
          productId,
          (item) => CartItem(
                id: item.id,
                productId: item.productId,
                title: item.title,
                quantity: item.quantity - 1,
                price: item.price,
              ));
    }
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }

  @override
  String toString() {
    return 'Cart{_items: $_items}';
  }
}
