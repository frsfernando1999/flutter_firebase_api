class LoginParameters{
  String email;
  String password;

  LoginParameters({
    required this.email,
    required this.password,
  });
}