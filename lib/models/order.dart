import 'models.dart';

class Order{
  final String id;
  final double total;
  final Set<CartItem> products;
  final DateTime dateTime;

  const Order({
    required this.id,
    required this.total,
    required this.products,
    required this.dateTime,
  });
}