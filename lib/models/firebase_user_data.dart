import 'dart:async';

import 'package:flutter/material.dart';

import '../store.dart';

class FirebaseUser with ChangeNotifier {
  String? userId;
  String? tokenId;
  String? localId;
  String? email;
  bool? emailVerified;
  String? createdAt;
  DateTime? expiresIn;
  List<String>? favorites;
  Timer? logoutTimer;

  final _logoutStream = StreamController<bool>.broadcast();
  Stream get logoutStream => _logoutStream.stream;

  @override
  void dispose(){
    _logoutStream.close();
    super.dispose();
  }

  FirebaseUser({
    this.userId,
    this.tokenId,
    this.localId,
    this.email,
    this.emailVerified,
    this.createdAt,
    this.expiresIn,
    this.favorites,
  });

  void logout() {
    userId = null;
    tokenId = null;
    localId = null;
    email = null;
    emailVerified = null;
    createdAt = null;
    expiresIn = null;
    favorites = null;
    logoutTimer?.cancel();
    logoutTimer = null;
    Store.remove('userData');
    _logoutStream.sink.add(true);
    notifyListeners();
  }

  void autoLogout() {
    logoutTimer?.cancel();
    logoutTimer = null;
    logoutTimer = Timer(Duration(seconds: 5), () => logout());
  }
}
