import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'models.dart';

class OrderList with ChangeNotifier {
  bool loading = false;
  String? token;
  String? userId;

  OrderList({this.token, this.userId}) {
    if(token != null) {
      getOrders(token, userId);
    }
    notifyListeners();
  }

  Future<void> getOrders(String? token, String? userId) async {
    loading = true;
    notifyListeners();
    try {
      print(userId);
      var response = await http.get(Uri.parse(
          'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com/orders/$userId.json?auth=$token'))
        ..body;
      var orders = jsonDecode(response.body) as Map;
      for (var order in orders.entries) {
        _items.add(Order(
          id: order.key,
          total: order.value["total"],
          products: _getProducts(order.value["products"]),
          dateTime: DateTime.parse(order.value['date']),
        ));
      }
    } catch (e) {
      print(e);
      print('Erro 1');
    } finally {
      loading = false;
      notifyListeners();
    }
  }

  Set<CartItem> _getProducts(var products) {
    Set<CartItem> list = {};
    for (var product in products) {
      var test = Map<String, dynamic>.from(product);
      list.add(CartItem(
          id: test['id'],
          productId: test['productId'],
          title: test['title'],
          quantity: test['quantity'],
          price: test['price']));
    }
    return list;
  }

  final Set<Order> _items = {};
  String? id;

  List<Order> get items {
    return [..._items];
  }

  int get itemsCount {
    return _items.length;
  }

  Future<void> addOrder(Cart cart, FirebaseUser user) async {
    loading = true;
    notifyListeners();
    try {
      await http
          .post(
              Uri.parse(
                  'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com/orders/${user.userId}.json?auth=$token'),
              body: jsonEncode({
                "total": cart.totalAmount,
                "products": cart.items.values
                    .map((cartItem) => {
                          "id": cartItem.id,
                          "productId": cartItem.productId,
                          "title": cartItem.title,
                          "price": cartItem.price,
                          "quantity": cartItem.quantity,
                        })
                    .toList(),
                "date": DateTime.now().toIso8601String(),
              }))
          .then((response) => id = jsonDecode(response.body)['name']);
      _items.add(
        Order(
          id: id as String,
          total: cart.totalAmount,
          products: cart.items.values.toSet(),
          dateTime: DateTime.now(),
        ),
      );
    } catch (e) {
      // TODO
      print('Erro 2');
    } finally {
      loading = false;
      notifyListeners();
    }
  }
}
