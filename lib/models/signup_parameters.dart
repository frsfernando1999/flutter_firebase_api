class SignUpParameters{
  String name;
  String email;
  String password;

  SignUpParameters({
    required this.name,
    required this.email,
    required this.password,
  });
}