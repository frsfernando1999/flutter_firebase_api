import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'models.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorite = false,
  });

  void toggleFavorite(FirebaseUser user, String prodId) async {
    const String baseUrl =
        'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com';
    await http.patch(
        Uri.parse(
            '$baseUrl/users/${user.localId}/${user.userId}.json?auth=${user.tokenId}'),
        body: jsonEncode({
          "favorites": {
            'favorite' : prodId,
          },
        }));
    isFavorite = !isFavorite;
    notifyListeners();
  }

  @override
  String toString() {
    return 'Product{id: $id, title: $title, description: $description, price: $price, imageUrl: $imageUrl, isFavorite: $isFavorite}';
  }
}
