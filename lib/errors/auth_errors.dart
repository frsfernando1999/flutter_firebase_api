class AuthError implements Exception{
  String error;

  static const Map<String, String> errors = {
    'EMAIL_NOT_FOUND': 'Senha ou email incorretos',
    'INVALID_PASSWORD': 'Senha ou email incorretos',
    'USER_DISABLED': 'Usuário temporariamente desabilitado',
    'EMAIL_EXISTS': 'Este email já é cadastrado',
    'OPERATION_NOT_ALLOWED': 'Operação não permitida',
    'TOO_MANY_ATTEMPTS_TRY_LATER': 'Seu login foi bloqueado após várias tentativas, tente novamente mais tarde.',
    'INVALID_ID_TOKEN': 'Erro inesperado',
    'USER_NOT_FOUND': 'Usuário não encontrado',
    'EMAIL_NOT_VERIFIED': 'Email ainda não verificado',
  };

  AuthError(
    this.error,
  );

  @override
  String toString() {
    return errors[error] ?? 'Erro inesperado, tente novamente mais tarde';
  }
}