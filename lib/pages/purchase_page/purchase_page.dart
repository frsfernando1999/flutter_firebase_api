import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';
import 'components/components.dart';

class PurchasePage extends StatelessWidget {
  const PurchasePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Cart cart = context.watch<Cart>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fechar Pedido'),
      ),
      body: Column(
        children: [
          Card(
            margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Chip(
                    label: Text(
                      'Total: R\$${cart.totalAmount}',
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    backgroundColor: Theme.of(context).colorScheme.primary,
                  ),
                  const Spacer(),
                  TextButton(
                      onPressed: cart.itemsCount == 0 ? null : () async {
                        await context.read<OrderList>().addOrder(cart, context.read<FirebaseUser>());
                        cart.clear();
                      },
                      child: const Text('FECHAR COMPRA')),
                ],
              ),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: cart.itemsCount,
                  itemBuilder: (_, index) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: CartOrderItem(cart: cart, index: index),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

