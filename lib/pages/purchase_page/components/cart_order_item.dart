import 'package:flutter/material.dart';

import '../../../models/models.dart';

class CartOrderItem extends StatelessWidget {
  const CartOrderItem({
    Key? key,
    required this.cart,
    required this.index,
  }) : super(key: key);

  final Cart cart;
  final int index;

  @override
  Widget build(BuildContext context) {
    final items = cart.items.values.toList();
    return Dismissible(
      background: Container(
        color: Colors.red,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.only(left: 12),
        child: const Icon(Icons.delete, color: Colors.white,),
      ),
      direction: DismissDirection.startToEnd,
      onDismissed: (_) => cart.removeItem(items[index].productId),
      confirmDismiss: (_) {
        return showDialog<bool>(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: const Text('Tem certeza?'),
                content: const Text(
                    'Deseja remover este item do carrinho?'),
                actions: [
                  TextButton(
                    onPressed: () =>
                        Navigator.of(context).pop(false),
                    child: const Text('Não'),
                  ),
                  TextButton(
                    onPressed: () =>
                        Navigator.of(context).pop(true),
                    child: const Text('Sim'),
                  ),
                ],
              );
            });
      },
      key: ValueKey(items[index].id),
      child: ListTile(
        leading: CircleAvatar(
          child: Text('${items[index].quantity}'),
        ),
        title: Text(items[index].title),
        subtitle: Text(
            'R\$${(items[index].price * items[index].quantity).toStringAsFixed(2)}'),
        trailing: IconButton(
            onPressed: () => cart
                .removeSingleItem(cart.items.keys.toList()[index]),
            icon: const Icon(Icons.remove)),
      ),
    );
  }
}
