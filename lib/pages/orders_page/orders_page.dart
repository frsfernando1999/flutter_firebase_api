import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';

class OrdersPage extends StatelessWidget {
  const OrdersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final OrderList orders = context.watch<OrderList>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pedidos'),
      ),
      body: orders.loading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: orders.itemsCount,
              itemBuilder: (context, index) {
                return Card(
                  child: ExpansionTile(
                    leading: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.primary,
                        borderRadius: const BorderRadius.all(Radius.circular(8)),
                      ),
                      padding:
                          const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                      child: Text(
                        DateFormat('dd/MM/yyyy')
                            .format(orders.items[index].dateTime),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    title: Text('Pedido código: ${orders.items[index].id}'),
                    subtitle: Text('Valor: R\$${orders.items[index].total}'),
                    children: [
                      ...orders.items[index].products.map((orderDetails) {
                        return ListTile(
                          title: Text(orderDetails.title),
                          leading: CircleAvatar(
                              child:
                                  Text('${orderDetails.quantity.toString()}x')),
                          trailing: Text(
                              'R\$${orderDetails.quantity * orderDetails.price}'),
                        );
                      }),
                    ],
                  ),
                );
              },
            ),
    );
  }
}
