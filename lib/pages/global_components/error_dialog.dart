import 'package:flutter/material.dart';

showErrorDialog(BuildContext context, String error) {
  showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text('Um erro ocorreu!'),
          content: Text('Erro: ${error}'),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Ok'))
          ],
        );
      });
}
