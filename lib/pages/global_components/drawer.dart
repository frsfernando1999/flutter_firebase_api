import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/models/models.dart';

import '../../utils/app_routes.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 8, bottom: 12),
            color: Theme.of(context).colorScheme.primary,
            child: const Text('Bem Vindo Usuário!',
            style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.shop),
            title: const Text('Loja'),
            onTap: (){
              Navigator.of(context).pushReplacementNamed(AppRoutes.homePage);
            },
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Pedidos'),
            onTap: (){
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(AppRoutes.ordersPage);
            },
          ),
          ListTile(
            leading: const Icon(Icons.edit),
            title: const Text('Produtos'),
            onTap: (){
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(AppRoutes.productManagementPage);
            },
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Sair'),
            onTap: (){
              context.read<FirebaseUser>().logout();
              Navigator.of(context).pushReplacementNamed(AppRoutes.loginPage);
            },
          ),
        ],
      ),
    );
  }

}
