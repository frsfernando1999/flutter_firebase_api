import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';
import '../../utils/app_routes.dart';

verifyUser(FirebaseUser? value, BuildContext context) async {
  if (value != null) {
    context.read<FirebaseUser>().tokenId = value.tokenId;
    context.read<FirebaseUser>().userId = value.userId;
    context.read<FirebaseUser>().expiresIn = value.expiresIn;
    context.read<FirebaseUser>().email = value.email;
    context.read<FirebaseUser>().createdAt = value.createdAt;
    context.read<FirebaseUser>().localId = value.localId;
    context.read<FirebaseUser>().emailVerified = value.emailVerified;

    if (value.emailVerified == true) {
      Navigator.of(context)
          .pushReplacementNamed(AppRoutes.homePage);
    } else {
      Navigator.of(context)
          .pushReplacementNamed(AppRoutes.confirmEmail);
    }
  }
}
