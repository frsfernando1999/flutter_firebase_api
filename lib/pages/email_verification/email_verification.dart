import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shop/errors/auth_errors.dart';

class EmailVerification {
  static const apiKey = 'AIzaSyBB3nkcVh2bXDT_EGy9kWbncfraH5zorUc';
  static const baseUrl =
      'https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key=$apiKey';
  static const baseGetUserUrl =
      'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=$apiKey';
  static const databaseUrl =
      'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com';

  Future<String> verifyEmail(String token) async {
    var response = await http.post(Uri.parse(baseUrl),
        body: jsonEncode({
          'requestType': "VERIFY_EMAIL",
          'idToken': token,
        }));
   Map<String, dynamic> email = jsonDecode(response.body);
   if(response.statusCode == 200) {
     return email['email'];
   }else{
     throw AuthError(email['error']['message']);
   }
  }

  Future<bool?> verifyVerification(String token, String userId) async {
    try {
      final response = await http.post(Uri.parse(baseGetUserUrl),
          body: jsonEncode({
            'idToken': token
          })
      );
      Map<String, dynamic> userData = jsonDecode(response.body);
      if(userData['users'][0]['emailVerified'] == true){
        final http.Response response = await http.get(Uri.parse('$databaseUrl/users/$userId.json?auth=$token'));
        Map<String, dynamic> responseData = jsonDecode(response.body);
        await http.patch(Uri.parse('$databaseUrl/users/$userId/${responseData.keys.first}.json?auth=$token'),
            body: jsonEncode({
              "emailVerified": true,
            }));
        return userData['users'][0]['emailVerified'];
      } else{
        throw AuthError('EMAIL_NOT_VERIFIED').toString();
      }
    }catch (e){
      throw AuthError('EMAIL_NOT_VERIFIED').toString();
    }
  }
}
