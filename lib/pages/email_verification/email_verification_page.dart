import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/models/models.dart';
import 'package:shop/pages/email_verification/email_verification.dart';
import 'package:shop/utils/app_routes.dart';

class EmailConfirmationPage extends StatelessWidget {
  const EmailConfirmationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FirebaseUser user = context.read<FirebaseUser>();
    _requestVerification(user.tokenId);
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirmação de Email'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Para prosseguir confirme seu e-mail',
              textAlign: TextAlign.center,
            ),
            TextButton(
                onPressed: () => _confirmVerification(user.tokenId, user.localId)
                    .then((value) => _redirect(value, user, context)),
                child: Text('Já conferi!'))
          ],
        ),
      ),
    );
  }

  Future<void> _requestVerification(String? token) async {
    EmailVerification emailVerification = EmailVerification();
    try {
      if (token != null) {
        await emailVerification.verifyEmail(token);
      }
    } catch (e) {
      // TODO
    }
  }

  Future<bool?> _confirmVerification(String? token, String? userId) async {
    EmailVerification emailVerification = EmailVerification();
    try {
      if (token != null && userId != null) {
        bool? verification =
            await emailVerification.verifyVerification(token, userId);
        if (verification == true) {
          return true;
        } else {
          return false;
        }
      }
    } catch (e) {
      // TODO
    }
  }

  _redirect(bool? value, FirebaseUser user, context) {
    if (value == true) {
      user.emailVerified = true;
      Navigator.of(context)
          .pushReplacementNamed(AppRoutes.homePage, arguments: user);
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Email ainda não verificado!')));
    }
  }
}
