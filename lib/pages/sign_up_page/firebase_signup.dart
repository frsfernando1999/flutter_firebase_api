import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:shop/models/models.dart';

import '../../errors/auth_errors.dart';

class FirebaseSignUp {
  static const apiKey = 'AIzaSyBB3nkcVh2bXDT_EGy9kWbncfraH5zorUc';
  static const baseUrl =
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$apiKey';
  static const databaseUrl =
      'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com';

  Future<FirebaseUser?> firebaseAuth(SignUpParameters parameters) async {
    try {
      final response = await http.post(
        Uri.parse(baseUrl),
        body: jsonEncode({
          'email': parameters.email,
          'password': parameters.password,
          'returnSecureToken': true
        }),
      );
      Map<String, dynamic> signup = jsonDecode(response.body);
      if (signup.containsKey('error')) {
        throw AuthError(signup['error']['message']).toString();
      }
      http.Response userData = await http.post(
          Uri.parse(
              'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=$apiKey'),
          body: jsonEncode({'idToken': signup['idToken']}));
      final userCompleteData = jsonDecode(userData.body);
      FirebaseUser user = FirebaseUser(
        tokenId: signup['idToken'],
        localId: userCompleteData['users'][0]['localId'],
        email: parameters.email,
        emailVerified: userCompleteData['users'][0]['emailVerified'],
        createdAt: userCompleteData['users'][0]['createdAt'],
        expiresIn: signup['expiresIn'],
      );
      await http.post(Uri.parse('$databaseUrl/users/${userCompleteData['users'][0]['localId']}.json?auth=${user.tokenId}'),
          body: jsonEncode({
            "email": parameters.email,
            "emailVerified": userCompleteData['users'][0]['emailVerified'],
            "name": parameters.name,
            "createdAt": userCompleteData['users'][0]['createdAt'],
          })).then((value) => user.userId = jsonDecode(value.body)['name']);
      user.autoLogout();
      return user;
    } catch (e) {
      rethrow;
    }
  }
}
