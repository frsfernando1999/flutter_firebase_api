import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/pages/sign_up_page/sign_up_validators.dart';
import 'package:shop/utils/app_routes.dart';

import '../../global_components/components.dart';

class AuthForm extends StatelessWidget {
  const AuthForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SignupValidators validators = context.watch<SignupValidators>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        const Text('Cadastro',
            style: TextStyle(fontSize: 26), textAlign: TextAlign.center),
        const SizedBox(height: 12),
        TextFormField(
          enabled: !validators.loading,
          onChanged: validators.validateName,
          keyboardType: TextInputType.name,
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            errorText: validators.nameError,
            label: const Text('Nome'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        TextFormField(
          enabled: !validators.loading,
          onChanged: validators.validateEmail,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            errorText: validators.emailError,
            label: const Text('Email'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        TextFormField(
          enabled: !validators.loading,
          onChanged: validators.validatePassword,
          obscureText: validators.visibility,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                onPressed: validators.toggleVisibility,
                icon: validators.visibility
                    ? const Icon(Icons.visibility_off)
                    : const Icon(Icons.visibility)),
            errorText: validators.passwordError,
            label: const Text('Senha'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        passwordRequirements1(context),
        passwordRequirements2(context),
        const SizedBox(height: 12),
        TextFormField(
          enabled: !validators.loading,
          onChanged: validators.validatePasswordConfirmation,
          obscureText: validators.visibility,
          autocorrect: false,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                onPressed: validators.toggleVisibility,
                icon: validators.visibility
                    ? const Icon(Icons.visibility_off)
                    : const Icon(Icons.visibility)),
            errorText: validators.passwordConfirmationError,
            label: const Text('Confirmar Senha'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.all(8),
            ),
            onPressed: validators.loading
                ? null
                : validators.formValid
                    ? () => validators
                        .auth()
                        .catchError((error) => showErrorDialog(context, error))
                        .then((value) => verifyUser(value, context))
                    : null,
            child: validators.loading
                ? const CircularProgressIndicator()
                : const Text('Cadastrar')),
        TextButton(
            onPressed: () =>
                Navigator.of(context).pushReplacementNamed(AppRoutes.loginPage),
            child: const Text('Já é cadastrado? Entre!'))
      ],
    );
  }
}

Row passwordRequirements1(BuildContext context) {
  SignupValidators validators = context.watch<SignupValidators>();
  return Row(
    children: [
      Flexible(
        flex: 1,
        child: ListTile(
          minLeadingWidth: 10,
          leading: validators.passwordSymbolError
              ? const Icon(Icons.unpublished, color: Colors.redAccent)
              : const Icon(Icons.check_circle, color: Colors.green),
          title: Text(
            'Deve conter 1 Simbolo',
            style: Theme.of(context).textTheme.subtitle2,
          ),
        ),
      ),
      Flexible(
          flex: 1,
          child: ListTile(
            minLeadingWidth: 10,
            leading: validators.passwordNumberError
                ? const Icon(Icons.unpublished, color: Colors.redAccent)
                : const Icon(Icons.check_circle, color: Colors.green),
            title: Text(
              'Deve conter 1 Número',
              style: Theme.of(context).textTheme.subtitle2,
            ),
          )),
    ],
  );
}

Row passwordRequirements2(BuildContext context) {
  SignupValidators validators = context.watch<SignupValidators>();
  return Row(
    children: [
      Flexible(
          flex: 1,
          child: ListTile(
            leading: validators.passwordUppercaseError
                ? const Icon(Icons.unpublished, color: Colors.redAccent)
                : const Icon(Icons.check_circle, color: Colors.green),
            minLeadingWidth: 10,
            title: Text(
              'Deve conter 1 letra maiuscula',
              style: Theme.of(context).textTheme.subtitle2,
            ),
          )),
      Flexible(
          flex: 1,
          child: ListTile(
            minLeadingWidth: 10,
            leading: validators.passwordLengthError
                ? const Icon(Icons.unpublished, color: Colors.redAccent)
                : const Icon(Icons.check_circle, color: Colors.green),
            title: Text(
              'Deve ter ao menos 8 letras',
              style: Theme.of(context).textTheme.subtitle2,
            ),
          )),
    ],
  );
}
