import 'package:flutter/foundation.dart';
import 'package:shop/models/models.dart';
import 'package:shop/pages/sign_up_page/firebase_signup.dart';

import '../../errors/auth_errors.dart';

class SignupValidators with ChangeNotifier {
  String? _name;
  String? _email;
  String? _password;

  String? nameError;
  String? emailError;
  String? passwordError;
  String? passwordConfirmationError;

  bool nameValid = false;
  bool emailValid = false;
  bool passwordValid = false;
  bool passwordConfirmationValid = false;

  bool visibility = true;

  bool passwordLengthError = true;
  bool passwordNumberError = true;
  bool passwordSymbolError = true;
  bool passwordUppercaseError = true;

  bool loading = false;

  void toggleVisibility() {
    visibility = !visibility;
    notifyListeners();
  }

  void validateName(String? name) {
    if (name != null) {
      if (name.trim().length >= 3) {
        _name = name.trim();
        nameError = null;
        nameValid = true;
        notifyListeners();
      } else {
        nameError = 'O nome deve ter mais de 3 letras!';
        nameValid = false;
        notifyListeners();
      }
    } else {
      nameError = 'Campo Obrigatório';
      nameValid = false;
      notifyListeners();
    }
  }

  void validateEmail(String? email) {
    if (email != null) {
      if (RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(email.trim())) {
        emailError = null;
        _email = email.trim();
        emailValid = true;
        notifyListeners();
      } else {
        emailError = 'Insira um email válido';
        emailValid = false;
        notifyListeners();
      }
    } else {
      emailError = 'Campo Obrigatório';
      emailValid = false;
      notifyListeners();
    }
  }

  void validatePassword(String? password) {
    if (password != null) {
      if (password.contains(
          RegExp(r'^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!@#\$&*~\.]).{8,}$'))) {
        passwordError = null;
        _password = password;
        passwordValid = true;
        notifyListeners();
      }
      if (password.length < 8) {
        passwordLengthError = true;
        passwordValid = false;
        notifyListeners();
      } else {
        passwordLengthError = false;
        notifyListeners();
      }
      if (!password.contains(RegExp(r'(?=.*?[!@#\$&*~\.])'))) {
        passwordSymbolError = true;
        passwordValid = false;
        notifyListeners();
      } else {
        passwordSymbolError = false;
        notifyListeners();
      }
      if (!password.contains(RegExp(r'(?=.*?[0-9])'))) {
        passwordNumberError = true;
        passwordValid = false;
        notifyListeners();
      } else {
        passwordNumberError = false;
        notifyListeners();
      }
      if (!password.contains(RegExp(r'(?=.*[A-Z])'))) {
        passwordUppercaseError = true;
        passwordValid = false;
        notifyListeners();
      } else {
        passwordUppercaseError = false;
        notifyListeners();
      }
    }
  }

  void validatePasswordConfirmation(String? passwordConfirmation) {
    if (_password == passwordConfirmation) {
      passwordConfirmationError = null;
      passwordConfirmationValid = true;
      notifyListeners();
    } else {
      passwordConfirmationError = 'As senhas não coincidem';
      passwordConfirmationValid = false;
      notifyListeners();
    }
  }

  bool get formValid =>
      nameValid && emailValid && passwordValid && passwordConfirmationValid;

  Future<FirebaseUser?> auth() async {
    loading = true;
    notifyListeners();
    try{
      SignUpParameters parameters = SignUpParameters(
        name: _name as String,
        email: _email as String,
        password: _password as String);
      FirebaseSignUp signUp = FirebaseSignUp();
      FirebaseUser? user = await signUp.firebaseAuth(parameters);
      return user;
    }catch (e){
      rethrow;
    }finally{
    loading = false;
    notifyListeners();
    }
  }
}
