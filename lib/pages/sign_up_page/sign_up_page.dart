import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/pages/sign_up_page/sign_up_validators.dart';

import 'components/auth_form.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Cadastro')),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
            Color.fromRGBO(180, 80, 240, 1),
            Color.fromRGBO(130, 100, 160, 1),
          ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              reverse: true,
              children: [
                Container(
                    height: MediaQuery.of(context).size.height * 0.20,
                    child: Image.asset('assets/images/online_shop.png')),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ChangeNotifierProvider(
                        create: (_) => SignupValidators(),
                        child: const AuthForm()),
                  ),
                ),
              ].reversed.toList(),
            ),
          ),
        ),
      )
    );
  }
}
