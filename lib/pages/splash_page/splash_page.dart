import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/models/models.dart';
import 'package:shop/store.dart';
import 'package:shop/utils/app_routes.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    _checkCache();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }

  Future<void> _checkCache() async {
    try {
      await Store.getMap('userData').then((userData) {
        print(userData);
        if (userData.isEmpty ||
            DateTime.now().isAfter(DateTime.parse(userData['expiresIn']))) {
          Navigator.of(context).pushReplacementNamed(AppRoutes.loginPage);
        } else {
          FirebaseUser user = context.read<FirebaseUser>();
          user.tokenId = userData['tokenId'];
          user.userId = userData['userId'];
          user.localId = userData['localId'];
          user.email = userData['email'];
          user.emailVerified = userData['emailVerified'];
          user.createdAt = userData['createdAt'];
          user.expiresIn = DateTime.parse(userData['expiresIn']);
          Navigator.of(context).pushReplacementNamed(AppRoutes.homePage);
        }
      });
    } catch (e) {
      Navigator.of(context).pushReplacementNamed(AppRoutes.loginPage);
    }
  }
}
