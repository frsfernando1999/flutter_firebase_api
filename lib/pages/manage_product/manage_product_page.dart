import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';
import '../../utils/app_routes.dart';

class ProductManagementPage extends StatelessWidget {
  const ProductManagementPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ProductList products = context.watch<ProductList>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Gerenciar Produtos'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(AppRoutes.productEditPage);
            },
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: ListView.builder(
            itemCount: products.items.length,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  ListTile(
                    leading: Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(const Radius.circular(12)),
                      ),
                      child: Image.network(products.items[index].imageUrl,
                          fit: BoxFit.cover, errorBuilder:
                              (BuildContext context, Object exception,
                                  StackTrace? stackTrace) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(Icons.error),
                            Text('Erro'),
                          ],
                        );
                      }),
                    ),
                    title: Text(products.items[index].title),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  AppRoutes.productEditPage,
                                  arguments: products.items[index]);
                            },
                            icon: const Icon(Icons.edit)),
                        IconButton(
                            onPressed: () => _showConfirmDialog(
                                context, products.items[index].id),
                            icon: const Icon(
                              Icons.delete,
                              color: Colors.red,
                            )),
                      ],
                    ),
                  ),
                  const Divider(),
                ],
              );
            }),
      ),
    );
  }

  _showConfirmDialog(BuildContext context, String id) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: const Text('Tem Certeza?'),
            content: const Text('Você deseja excluir este produto?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    ProductList list = context.read<ProductList>();
                    list.removeProduct(id);
                  },
                  child: const Text('Sim')),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Não')),
            ],
          );
        });
  }
}
