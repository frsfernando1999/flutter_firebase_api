import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../models/product.dart';
import '../../models/product_list.dart';
import 'product_edit_validators.dart';

class ProductEditPage extends StatelessWidget {
  const ProductEditPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Product? product = ModalRoute.of(context)?.settings.arguments as Product?;
    bool prodExists = !(product == null);
    Size mediaQuery = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
      create: (_) => ProductEditState(
        id: product?.id,
        name: product?.title,
        description: product?.description,
        imageUrl: product?.imageUrl,
        price: product?.price,
        isFavorite: product?.isFavorite,
      ),
      child: Scaffold(
        appBar: AppBar(
          title: prodExists ? Text(product.title) : const Text('Novo Produto'),
        ),
        body: Builder(builder: (context) {
          ProductEditState productValidations =
              context.watch<ProductEditState>();
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    prodExists ? 'Editar ${product.title}' : 'Novo Produto',
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 25),
                  ),
                  const Divider(),
                  TextFormField(
                    enabled: !productValidations.loading,
                    onChanged: productValidations.nameValidation,
                    decoration: InputDecoration(
                        errorText: productValidations.nameError,
                        label: const Text('Nome do Produto'),
                        border: const OutlineInputBorder(),
                        isDense: true),
                    initialValue: product?.title,
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 12),
                  TextFormField(
                    enabled: !productValidations.loading,
                    onChanged: productValidations.priceValidation,
                    decoration: InputDecoration(
                      label: const Text('Preço'),
                      errorText: productValidations.priceError,
                      border: const OutlineInputBorder(),
                      isDense: true,
                      prefixText: 'R\$ ',
                    ),
                    initialValue: product?.price.toString(),
                    keyboardType: const TextInputType.numberWithOptions(),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(
                          RegExp(r'^\d+([\.,])?\d{0,2}')),
                    ],
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 12),
                  TextFormField(
                    enabled: !productValidations.loading,
                    maxLines: 5,
                    onChanged: productValidations.descriptionValidation,
                    minLines: 1,
                    decoration: InputDecoration(
                      errorText: productValidations.descriptionError,
                      label: const Text('Descrição'),
                      border: const OutlineInputBorder(),
                      isDense: true,
                    ),
                    maxLength: 100,
                    initialValue: product?.description,
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 12),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 6,
                        child: TextFormField(
                          enabled: !productValidations.loading,
                          onChanged: productValidations.urlValidation,
                          maxLines: 5,
                          minLines: 1,
                          decoration: InputDecoration(
                            errorText: productValidations.imageUrlError,
                            label: const Text('URL da Imagem'),
                            border: const OutlineInputBorder(),
                            isDense: true,
                          ),
                          initialValue: product?.imageUrl,
                          textInputAction: TextInputAction.next,
                        ),
                      ),
                      const SizedBox(width: 12),
                      Flexible(
                        flex: 4,
                        child: Container(
                            color: Colors.white,
                            alignment: Alignment.center,
                            height: mediaQuery.height * 0.2,
                            child: Image.network(
                              productValidations.imageUrl ?? 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg',
                              errorBuilder: (BuildContext context,
                                  Object exception, StackTrace? stackTrace) {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Icon(Icons.error),
                                    Text('Erro'),
                                  ],
                                );
                              },
                            )),
                      ),
                    ],
                  ),
                  const SizedBox(height: 12),
                  ElevatedButton(
                      onPressed: productValidations.loading
                          ? null
                          : () {
                              productValidations
                                  .formValid(context.read<ProductList>())
                                  ?.then((value) {
                                if (value == true) {
                                  Navigator.of(context).pop();
                                }
                                if(value == false){
                                  _showErrorDialog(context);
                                }
                              });
                            },
                      child: productValidations.loading
                          ? const CircularProgressIndicator()
                          : const Text('Salvar Produto')),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  void _showErrorDialog(BuildContext context) {
    showDialog(context: context, builder: (_){
      return AlertDialog(
        title: const Text('Falha ao enviar dados'),
        content: const Text('Ocorreu um erro invesperado com sua requisição, tente novamente mais tarde.'),
        actions: [
          TextButton(onPressed: () => Navigator.of(context).pop(), child: const Text('Ok'))
        ],
      );
    });
  }
}
