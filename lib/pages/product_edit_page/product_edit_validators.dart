import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shop/models/product.dart';

import '../../models/product_list.dart';

class ProductEditState with ChangeNotifier {
  ProductEditState({
    this.id,
    this.name,
    this.description,
    this.imageUrl,
    this.price,
    this.isFavorite,
  }) {
    if (id != null) {
      nameValid = true;
      priceValid = true;
      descriptionValid = true;
      urlValid = true;
    }
  }

  String? id;

  String? name;
  String? nameError;
  double? price;
  String? priceError;
  String? description;
  String? descriptionError;
  String? imageUrl;
  String? imageUrlError;
  bool? isFavorite;

  bool? nameValid;
  bool? priceValid;
  bool? descriptionValid;
  bool? urlValid;

  bool loading = false;

  void nameValidation(String? name) {
    this.name = name;
    if (name == null || name.trim().length < 3) {
      nameError = 'O nome deve ter mais de 3 letras!';
      nameValid = false;
      notifyListeners();
    } else {
      nameError = null;
      nameValid = true;
      notifyListeners();
    }
  }

  void priceValidation(String? price) {
    if (price != null && price.isNotEmpty) {
      double dotPrice = double.parse(price.replaceAll(',', '.'));
      this.price = dotPrice;
      if (dotPrice > 0 && dotPrice <= 1000000) {
        priceError = null;
        priceValid = true;
        notifyListeners();
      } else {
        priceError = 'O valor deve ser entre 0 e um milhão!';
        priceValid = false;
        notifyListeners();
      }
    } else if (price == null || price == '') {
      priceError = 'Campo Obrigatório';
      priceValid = false;
      notifyListeners();
    } else {
      priceValid = false;
      notifyListeners();
    }
  }

  void descriptionValidation(String? description) {
    this.description = description;
    if (description == null || description.trim().length < 10) {
      descriptionError = 'A descrição deve ter ao menos 10 letras!';
      descriptionValid = false;
      notifyListeners();
    } else {
      descriptionError = null;
      descriptionValid = true;
      notifyListeners();
    }
  }

  void urlValidation(String? url) {
    imageUrl = url;
    if (url != null &&
        url.trim().isNotEmpty &&
        Uri.tryParse(url)!.hasAbsolutePath) {
      if (url.toLowerCase().endsWith('.png') ||
          url.toLowerCase().endsWith('.jpg') ||
          url.toLowerCase().endsWith('.jpeg')||
          url.toLowerCase().endsWith('300')
      ) {
        urlValid = true;
        imageUrlError = null;
        notifyListeners();
      } else {
        urlValid = false;
        imageUrlError = 'Url Inválida';
        notifyListeners();
      }
    } else {
      urlValid = false;
      imageUrlError = 'Campo Inválido';
      notifyListeners();
    }
  }

  Future<bool?>? formValid(ProductList productList) async {
    if (nameValid == true &&
        descriptionValid == true &&
        priceValid == true &&
        urlValid == true) {
      const String baseUrl =
          'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com';
      loading = true;
      notifyListeners();
      if (id == null) {
        try {

          await http
              .post(Uri.parse('$baseUrl/products.json'),
                  body: jsonEncode({
                    "title": name,
                    "description": description,
                    "price": price,
                    "imageUrl": imageUrl,
                    "isFavorite": false,
                  }))
              .then((resp) => id = jsonDecode(resp.body)['name']);
          productList.addProduct(
            Product(
              id: id as String,
              title: name as String,
              description: description as String,
              price: price as double,
              imageUrl: imageUrl as String,
            ),
          );
        } catch (e) {
          return false;
        }
      } else {
        try {
          await http.put(Uri.parse('$baseUrl/products/$id.json'),
              body: jsonEncode({
                "title": name,
                "description": description,
                "price": price,
                "imageUrl": imageUrl,
                "isFavorite": isFavorite,
              }));
          productList.items.removeWhere((item) => item.id == id);
          productList.addProduct(Product(
              id: id as String,
              title: name as String,
              description: description as String,
              price: price as double,
              imageUrl: imageUrl as String));
        } catch (e) {
          return false;
        }
      }
      loading = false;
      notifyListeners();
      return true;
    } else {
      urlValidation(imageUrl);
      nameValidation(name);
      descriptionValidation(description);
      priceValidation(price?.toString());
    }
    return null;
  }
}
