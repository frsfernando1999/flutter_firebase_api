import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/pages/login_page/login_validators.dart';

import 'components/components.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Login'),
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
            Color.fromRGBO(180, 80, 240, 1),
            Color.fromRGBO(130, 100, 160, 1),
          ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            reverse: true,
            children: [
              Container(
                  height: MediaQuery.of(context).size.height * 0.20,
                  // transform: Matrix4.rotationZ(-8 * pi / 180)..translate(-10),
                  child: Image.asset('assets/images/online_shop.png')),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ChangeNotifierProvider(
                      create: (_) => LoginValidators(),
                      child: const AuthForm()),
                ),
              ),
            ].reversed.toList(),
          ),
        ),
      ),
    );
  }
}
