import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/pages/login_page/login_validators.dart';
import 'package:shop/utils/app_routes.dart';

import '../../global_components/components.dart';

class AuthForm extends StatelessWidget {
  const AuthForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LoginValidators validators = context.watch<LoginValidators>();
    return ListView(
      shrinkWrap: true,
      children: [
        const Text('Login',
            style: TextStyle(fontSize: 26), textAlign: TextAlign.center),
        const SizedBox(height: 12),
        TextFormField(
          enabled: !validators.loading,
          onChanged: validators.validateEmail,
          initialValue: kDebugMode ? 'favase6791@teasya.com' : null,
          decoration: InputDecoration(
            errorText: validators.emailError,
            label: const Text('Email'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        TextFormField(
          initialValue: kDebugMode ? 'Fernando.1' : null,
          enabled: !validators.loading,
          obscureText: validators.visibility,
          onChanged: validators.validatePassword,
          decoration: InputDecoration(
            suffixIcon: IconButton(
                onPressed: validators.toggleVisibility,
                icon: validators.visibility
                    ? const Icon(Icons.visibility_off)
                    : const Icon(Icons.visibility)),
            errorText: validators.passwordError,
            label: const Text('Senha'),
            border: const OutlineInputBorder(),
          ),
          textInputAction: TextInputAction.next,
        ),
        const SizedBox(height: 12),
        Column(
          children: [
            AnimatedContainer(
              width: validators.loading
                  ? 50
                  : MediaQuery.of(context).size.width * 0.95,
              duration: Duration(milliseconds: 500),
              curve: Curves.linear,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.all(8),
                  ),
                  onPressed: validators.loading
                      ? null
                      : validators.formValid
                          ? () => validators
                              .auth()
                              .catchError(
                                  (error) => showErrorDialog(context, error))
                              .then((value) => verifyUser(value, context))
                          : null,
                  child: validators.loading
                      ? const CircularProgressIndicator()
                      : const Text('Entrar')),
            ),
            TextButton(
                onPressed: () => Navigator.of(context)
                    .pushReplacementNamed(AppRoutes.signUpPage),
                child: const Text('Não é cadastrado? Inscreva-se!'))
          ],
        ),
      ],
    );
  }
}
