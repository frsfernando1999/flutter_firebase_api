import 'dart:convert';

import 'package:shop/errors/auth_errors.dart';
import 'package:shop/models/models.dart';

import 'package:http/http.dart' as http;

import '../../store.dart';

class FirebaseLogin {
  static const apiKey = 'AIzaSyBB3nkcVh2bXDT_EGy9kWbncfraH5zorUc';
  static const baseUrl =
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$apiKey';
  static const databaseUrl =
      'https://asdasdsadsasdadsad-default-rtdb.firebaseio.com';

  Future<FirebaseUser?> firebaseAuth(LoginParameters parameters) async {
    try {
      final response = await http.post(Uri.parse(baseUrl),
          body: jsonEncode({
            'email': parameters.email,
            'password': parameters.password,
            'returnSecureToken': true
          }));
      Map<String, dynamic> login = jsonDecode(response.body);
      if (login.containsKey('error')) {
        throw AuthError(login['error']['message']).toString();
      }
      http.Response userData = await http.post(
          Uri.parse(
              'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=$apiKey'),
          body: jsonEncode({'idToken': login['idToken']}));
      final userCompleteData = jsonDecode(userData.body);
      FirebaseUser user = FirebaseUser(
        tokenId: login['idToken'],
        localId: userCompleteData['users'][0]['localId'],
        email: parameters.email,
        emailVerified: userCompleteData['users'][0]['emailVerified'],
        createdAt: userCompleteData['users'][0]['createdAt'],
        expiresIn: DateTime.now().add(Duration(seconds: int.parse(login['expiresIn']))),
      );
      final http.Response userIdRequest = await http.get(Uri.parse('$databaseUrl/users/${user.localId}.json?auth=${user.tokenId}'));
      Map<String, dynamic> userIdRequestDecoded =  jsonDecode(userIdRequest.body);
      String userId = userIdRequestDecoded.keys.first;
      print(userIdRequestDecoded);
      user.userId = userId;
      await Store.saveMap(
          'userData', {
        'tokenId': user.tokenId,
        'emailVerified': user.emailVerified,
        'email': user.email,
        'expiresIn': user.expiresIn?.toIso8601String(),
        'createdAt': user.createdAt,
        'userId': user.userId,
        'localId': user.localId,
      });
      return user;
    } catch (e) {
      rethrow;
    }
  }
}
