import 'package:flutter/foundation.dart';

import '../../models/models.dart';
import 'firebase_login.dart';

class LoginValidators with ChangeNotifier {
  String? _email;
  String? _password;

  String? emailError;
  String? passwordError;

  bool emailValid = false;
  bool passwordValid = false;

  bool visibility = true;

  bool loading = false;

  void validateEmail(String? email) {
    if(email != null){
      if(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email.trim())){
        emailError = null;
        _email = email.trim();
        emailValid = true;
        notifyListeners();
      }else{
        emailError = 'Insira um email válido';
        emailValid = false;
        notifyListeners();
      }
    }else{
      emailError = 'Campo Obrigatório';
      emailValid = false;
      notifyListeners();
    }
  }

  void toggleVisibility(){
    visibility = !visibility;
    notifyListeners();
  }

  void validatePassword(String? password) {
    if(password != null){
      if(password.trim().length >= 5){
        _password = password.trim();
        passwordError = null;
        passwordValid = true;
        notifyListeners();
      }else{
        passwordError = 'A senha deve conter ao menos 5 letras';
        passwordValid = false;
        notifyListeners();
      }
    }else{
      passwordError = 'Campo Obrigatorio!';
      passwordValid = false;
      notifyListeners();
    }
  }

  bool get formValid => emailValid && passwordValid;

  Future<FirebaseUser?> auth() async {
    loading = true;
    notifyListeners();
    try{
      LoginParameters parameters = LoginParameters(
          email: _email as String,
          password: _password as String);
      FirebaseLogin login = FirebaseLogin();
      FirebaseUser? user = await login.firebaseAuth(parameters);
        return user;
    }catch (e){
     rethrow;
    }finally{
    loading = false;
    notifyListeners();
    }
  }

  void loadTest() async{
    loading = true;
        notifyListeners();
    await Future.delayed(Duration(seconds: 2));
    loading = false;
        notifyListeners();

  }
}
