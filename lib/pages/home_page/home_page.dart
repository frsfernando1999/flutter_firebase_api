import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:shop/utils/app_routes.dart';

import '../global_components/drawer.dart';
import '../../models/models.dart';
import './components/components.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  void _openCart() {
    showDialog(
        context: context,
        builder: (_) {
          return const CartDialog();
        });
  }

  @override
  Widget build(BuildContext context) {
        ProductList products = context.watch<ProductList>();
        context.read<FirebaseUser>().logoutStream.listen((logout){
          if(logout){
            Navigator.pushReplacementNamed(context, AppRoutes.loginPage);
          }
        });
    return  Scaffold(
          appBar: AppBar(
            actions: [
              CartBadge(onTap: _openCart),
              PopupMenuButton(
                itemBuilder: (_) {
                  return [
                    const PopupMenuItem(
                        value: 0, child: Text('Somente Favoritos')),
                    const PopupMenuItem(value: 1, child: Text('Todos')),
                  ];
                },
                onSelected: (int selectedValue) =>
                    products.toggleFavorites(selectedValue),
              ),
            ],
            title: const Text('Loja'),
            centerTitle: true,
          ),
          body: products.loading
              ? const Center(
                  child: const CircularProgressIndicator(),
                )
              : GridView.builder(
                  padding: const EdgeInsets.all(8),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.5,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: products.items.length,
                  itemBuilder: (context, index) {
                    return ChangeNotifierProvider.value(
                      value: products.items[index],
                      child: ProductItem(
                        key: ValueKey(products.items[index].id),
                      ),
                    );
                  }),
          drawer: const CustomDrawer(),
        );
      }
  }

