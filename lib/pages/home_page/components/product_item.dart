import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/utils/app_routes.dart';

import '../../../models/models.dart';

class ProductItem extends StatelessWidget {
  const ProductItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Product product = context.read<Product>();
    final Cart cart = context.read<Cart>();
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context)
              .pushNamed(AppRoutes.productDetails, arguments: product);
        },
        child: GridTile(
          footer: GridTileBar(
            backgroundColor: Colors.black54,
            leading: Consumer<Product>(builder: (context, product, _) {
              return IconButton(
                onPressed: () => product.toggleFavorite(
                    context.read<FirebaseUser>(), product.id),
                icon: product.isFavorite
                    ? const Icon(Icons.favorite)
                    : const Icon(Icons.favorite_border),
              );
            }),
            trailing: IconButton(
              onPressed: () => cart.addItem(product),
              icon: const Icon(Icons.shopping_cart),
            ),
            title: Text(
              product.title,
              overflow: TextOverflow.clip,
              style: const TextStyle(),
            ),
          ),
          child: Hero(
            tag: product.id,
            child: FadeInImage(
              image: NetworkImage(product.imageUrl),
              placeholder: AssetImage('assets/images/no_image.png'),
            ),
          ),
        ),
      ),
    );
  }
}
