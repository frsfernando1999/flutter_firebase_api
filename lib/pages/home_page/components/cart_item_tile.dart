import 'package:flutter/material.dart';

import '../../../models/models.dart';

class CartItemTile extends StatelessWidget {
  const CartItemTile({
    Key? key,
    required this.cartItem,
    required this.onDelete,
    required this.id,
  }) : super(key: key);

  final CartItem cartItem;
  final void Function(String) onDelete;
  final String id;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: CircleAvatar(child: Text('${cartItem.quantity}')),
          title: Text(cartItem.title),
          trailing: IconButton(
            icon: const Icon(Icons.remove),
            onPressed: () => onDelete(id),
          ),
          subtitle: Text('R\$${cartItem.price * cartItem.quantity}'),
        ),
        const Divider(),
      ],
    );
  }
}
