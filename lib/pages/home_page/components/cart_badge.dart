import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/models.dart';

class CartBadge extends StatelessWidget {
  const CartBadge({required this.onTap, Key? key}) : super(key: key);

  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizedBox(
        width: 40,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            const Icon(Icons.shopping_cart),
            Positioned(
              top: 4,
              right: 0,
              child: SizedBox(
                height: 20,
                child: CircleAvatar(
                  backgroundColor: Colors.red,
                  child: Text(
                    '${context.watch<Cart>().cartTotalItems}',
                    style: const TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
