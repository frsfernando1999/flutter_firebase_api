import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../utils/app_routes.dart';
import '../../../models/models.dart';
import './components.dart';

class CartDialog extends StatelessWidget {
  const CartDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Cart cart = context.watch<Cart>();
    Size deviceSizes = MediaQuery.of(context).size;
    return SimpleDialog(
      insetPadding: EdgeInsets.zero,
      contentPadding: EdgeInsets.zero,
      children: [
        SizedBox(
          width: deviceSizes.width * 0.8,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  'Carrinho de Compras',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              Container(
                constraints: const BoxConstraints(maxHeight: 300),
                child: ListView(
                  children: createCartList(cart),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0, top: 8.0, right: 8),
                child: Text('Total: R\$${cart.totalAmount}', style: const TextStyle(fontSize: 18),textAlign: TextAlign.right,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: cart.clear,
                        child: const Text('Limpar carrinho?')),
                    ElevatedButton(
                        onPressed: () => _purchasePage(context),
                        child: const Text('Fechar Compra')),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _purchasePage(context) {
    Navigator.of(context).pop();
    Navigator.of(context).pushNamed(AppRoutes.purchasePage);
  }

  List<Widget> createCartList(Cart cart) {
    List<Widget> list = [];
    for (var entries in cart.items.entries) {
      var cartItem = entries.value;
      list.add(
        CartItemTile(
          cartItem: cartItem,
          onDelete: cart.removeSingleItem,
          id: entries.key,
        ),
      );
    }
    return list;
  }
}
