# Firebase Realtime Database API project

The purpose of this project was to learn about Provider, a few animations, using the Firebase API to authenticate users and write data.

## The idea is pretty simple:

1. You Sign up
2. You validate your email (a fake email was used for testing)
3. Once you get authenticated and your email is validated, it adds your token and data on the cache
4. When you sign in, your token expiry date is checked, if it is still valid, you'll be automatically redirected to the home page
5. On the Home page, you can add products to you cart, go to edit a product page or orders page
6. Every time you create a new product or order, it is sent to firebase via the API.
